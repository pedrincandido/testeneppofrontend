import { Component, OnInit } from '@angular/core';
import { PersonService } from './person.service';
import { PersonViewModel } from '../viewModels/person.view-model';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  providers: [PersonService]
})
export class PersonComponent implements OnInit {

  personData: PersonViewModel = new PersonViewModel({});
  executePost: boolean;
  personForm: FormGroup;
  person_id: number;

  constructor(
    // private router: Router,
    private formBuilder: FormBuilder,
    private personService: PersonService,
    private rota: ActivatedRoute

  ) { this.person_id = rota.snapshot.params['id']; }

  ngOnInit() {
    this.buildForm();
    this.getPersonByPersonId();
  }

  buildForm() {
    this.personForm = this.formBuilder.group({
      'nome': ['', Validators.required],
      'cpf': ['', Validators.required],
      'dataNascimento': ['', Validators.required],
      'sexo': ['', Validators.required]
    });
  }


  getPersonByPersonId() {

    const idPerson = this.person_id;
    this.personService.getPersonById(idPerson).subscribe(
      (data: any) => {
        if (data !== undefined) {
          if (data.id !== null) {
            this.personData = new PersonViewModel(data);
            this.personData.id = parseInt(data.id, 10);
          }
        }
      },
      err => {
        const errorSave = err;
        if (errorSave.errors != null) {
          if (errorSave.errors[0].Status === 404) {
            this.executePost = true;
          }
        }
      });
  }


  saveData() {
    debugger
    if (this.personData.id != null) {
      this.executePost = false;
    }

    if (this.executePost) {
      this.personService.postPerson(this.personData).subscribe(result => {
        if (result) {
          const dataReturn = result.Data;
        }
      });
    }
  }

  submitUpdate() {
    this.personService.updatePerson(this.personData).subscribe(result => {
      alert("Atualizado com sucesso");
    }, error => {
      var data = JSON.parse(error._body);
    });
  }

  validContinue() {
    if (this.personForm.valid) {
      this.saveData();
    }
  }

  parseDataSend() {

  }

  disableNext() {

  }
}
