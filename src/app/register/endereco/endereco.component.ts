import { Component, OnInit } from '@angular/core';
import { EnderecoViewModel } from '../viewModels/endereco.view-model';
import { EnderecoService } from './endereco.service';
import { ActivatedRoute } from '@angular/router';



@Component({
  selector: 'app-endereco',
  templateUrl: './endereco.component.html',
  providers: [EnderecoService]
})
export class EnderecoComponent implements OnInit {

  enderecoData: EnderecoViewModel = new EnderecoViewModel({}); 
  endereco_id : number;

  executePost: boolean;
  constructor (
        private enderecoService: EnderecoService,
        private rota: ActivatedRoute
      ) 
      { this.endereco_id = rota.snapshot.params['id']; }

  ngOnInit() {
  }


  getEnderecoById() {

    const idEndereco = this.endereco_id;
    this.enderecoService.getEnderecoById(idEndereco).subscribe(
      (data: any) => {
        if (data !== undefined) {
          if (data.id !== null) {
            this.enderecoData = new EnderecoViewModel(data);
            this.enderecoData.id = parseInt(data.id, 10);
          }
        }
      },
      err => {
        const errorSave = err;
        if (errorSave.errors != null) {
          if (errorSave.errors[0].Status === 404) {
            this.executePost = true;
          }
        }
      });
  }


  saveData() {

    if (this.enderecoData.id != null) {
      this.executePost = false;
    }

    if (this.executePost) {
      this.enderecoService.postEndereco(this.enderecoData).subscribe(result => {
        if (result) {
          const dataReturn = result.Data;
        }
      });
    }
  }

  submitUpdate() {
    this.enderecoService.updateEndereco(this.enderecoData).subscribe(result => {
      alert("Atualizado com sucesso");
    }, error => {
      var data = JSON.parse(error._body);
    });
  }

}
